package meals.MpesaPush;


import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class MpesaPay
{
	@Autowired
	public UserData userdata;
	
	String appKey;
    String appSecret;
    
    public MpesaPay() {}
    
    public MpesaPay(String app_key, String app_secret)
    {
        appKey=app_key;
        appSecret=app_secret;
    }
    
    
    
    public String authenticate() 
    {
        String app_key = appKey/*"GvzjNnYgNJtwgwfLBkZh65VPwfuKvs0V"*/;
        String app_secret = appSecret;
        String appKeySecret = app_key + ":" + app_secret;
        byte[] bytes = null;
		try {
			bytes = appKeySecret.getBytes("ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String encoded = Base64.getEncoder().encodeToString(bytes);


        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
        		
                .url("https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
                .get()
                .addHeader("authorization", "Basic "+encoded)
                .addHeader("cache-control", "no-cache")

                .build();

        Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(response.body().string());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println(jsonObject.getString("access_token"));
        return jsonObject.getString("access_token");
    }
    

    public String STKPushSimulation(String businessShortCode, String password, String timestamp,
    		String transactionType, String amount, String phoneNumber, 
    		String partyA, String partyB, String callBackURL, String queueTimeOutURL,
    		String accountReference, String transactionDesc)
    {
        JSONArray jsonArray=new JSONArray();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("BusinessShortCode", businessShortCode);
        jsonObject.put("Password", password);
        jsonObject.put("Timestamp", timestamp);
        jsonObject.put("TransactionType", transactionType);
        jsonObject.put("Amount",amount);
        jsonObject.put("PhoneNumber", phoneNumber);
        jsonObject.put("PartyA", partyA);
        jsonObject.put("PartyB", partyB);
        jsonObject.put("CallBackURL", callBackURL);
        jsonObject.put("AccountReference", accountReference);
        jsonObject.put("QueueTimeOutURL", queueTimeOutURL);
        jsonObject.put("TransactionDesc", transactionDesc);



        jsonArray.put(jsonObject);

        String requestJson=jsonArray.toString().replaceAll("[\\[\\]]","");

        OkHttpClient client = new OkHttpClient();
       // https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials
        String url="https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest";
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, requestJson);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("authorization", "Bearer "+authenticate())
                .addHeader("cache-control", "no-cache")
                .build();


        Response response = null;
		try {
			response = client.newCall(request).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			System.out.println(response.body().string());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response.body().toString();
    }
    

    public static void main(String[] args)
    {
    	//Scanner input = new Scanner(System.in);
    	UserData us = new UserData();
    	us.setPhone(us.getPhone());
    	//System.out.println("Enter your phone number:");
    	//String phone = input.nextLine();
    	String phone = us.getPhone();
    	
    	MpesaPay m = new MpesaPay("3C5yDqioxtPc1geiIx4smuUZTKznAnGu","7K1WmIEZLyRSVH7f");
        m.authenticate();

        m.STKPushSimulation("174379",
				"MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTkwNDIyMTczNjUy",
				"20190422173652",
				"CustomerPayBillOnline",
				"1",
				phone, phone,
				
				"174379",
				"http://mpesa-requestbin.herokuapp.com/12jim7f1",
				"http://mpesa-requestbin.herokuapp.com/12jim7f1",
				"Mtugo",
				"Meal");

        //input.close();
    }
}
