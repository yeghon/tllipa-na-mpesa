<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

table, th, td 
	{
		border: 1px solid black;
		border-collapse: collapse;
		font-size:30px;
		
	}
	th
	{
		padding: 5px;
		text-align: center;
	}
	td
	{
		padding-left: 25px;
		text-align: left;
	}


body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.button {
  background-color: #9ACD32; /* Green */
  border: none;
  color: white;
  padding: 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-weight: bold;
  font-size: 15px;
  margin: 4px 2px;
  cursor: pointer;
  width: 90px;
}
.button4 {border-radius: 6px;}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("meals.jpeg");
  
  /* Add the blur effect */
  filter: blur(8px);
  -webkit-filter: blur(8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
}
</style>
</head>
<body>

<div class="bg-image"></div>

<div class="bg-text">
  
<h1 style = "text-align: left"><b><i>Mtugo Meals</i></b></h1>
<p style = "text-align: left"><i>

<%= (new java.util.Date()).toLocaleString()%></i></p>
		<table style="width:100%">
  			<caption>We have the following meals</caption>
  			
  			<tr>
  				<th>Meal</th>
  				<th>Cost</th>
  				<th>Quantity</th>
  				<th>Buy</th>
  			</tr>
  			
  			<tr>
  				<td>Spaghetti</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>

					<script>
						function mpesaPrompt() 
						{
							var phone;
							phone = prompt("Please enter your M-Pesa number, Strictly", "254728626789");
							
							if (phone === "")
								{	
									alert("Sorry, you need to enter a phone number");									
								}
							else if (phone.length != 12)
								{
									alert("Invalid phone number");
								}
							else if (isNaN(phone))
								{
									alert("Sorry, that's not a number");
								}
							else 
								{
									proceed(phone);
								}
							
						}
						
						function proceed(number)
						{	
							  if (confirm("Meal charge to be deducted from " + number + " ?")) 
							  {
								 
								  //consume mpesa push stk
								  alert("Enjoy your meal");
							  }
							  else 
							  {
								  alert("We could not process your request, try again!!");
							  }
						}
					</script>
					  				
  				
  					<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  				</td>
  			</tr>
  			
  			<tr>
  				<td>Pizza</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  						
  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>KFC</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  				
  					<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>Coffee</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					
  						<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>Milk</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					
  						<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>Meat</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					
  						<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>Burger</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					
  						<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					
  				</td>
  			</tr>
  			
  			<tr>
  				<td>HotDog</td>
  				<td>Sh. 1.00</td>
  				<td><button>1</button></td>
  				<td>
  					<form action="checkout.jsp">
  						<button class="button button4" name = "phonenumber">Checkout</button>
  					</form>
  					
  					
  				</td>
  			</tr>	
  
</table>



</div>

</body>
</html>
